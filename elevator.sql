-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 06 2018 г., 15:16
-- Версия сервера: 5.6.31
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `elevator`
--

-- --------------------------------------------------------

--
-- Структура таблицы `elevator`
--

CREATE TABLE IF NOT EXISTS `elevator` (
  `id` int(11) NOT NULL,
  `current_floor` int(2) NOT NULL,
  `direction` varchar(10) COLLATE utf8_bin NOT NULL,
  `door` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `elevator`
--

INSERT INTO `elevator` (`id`, `current_floor`, `direction`, `door`) VALUES
(1, 1, 'stand', 'closed'),
(2, 1, 'stand', 'closed'),
(3, 1, 'stand', 'closed'),
(4, 1, 'stand', 'closed');

-- --------------------------------------------------------

--
-- Структура таблицы `elevator_request`
--

CREATE TABLE IF NOT EXISTS `elevator_request` (
  `id` int(11) NOT NULL,
  `from_floor` int(2) NOT NULL,
  `to_floor` int(2) NOT NULL,
  `elevator_id` int(11) NOT NULL,
  `processing` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `elevator_request`
--

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `elevator`
--
ALTER TABLE `elevator`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `elevator_request`
--
ALTER TABLE `elevator_request`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `elevator`
--
ALTER TABLE `elevator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `elevator_request`
--
ALTER TABLE `elevator_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
