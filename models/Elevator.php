<?php
 
namespace app\models;
 
use Yii;
 
class Elevator extends \yii\db\ActiveRecord
{	
    const DIRECTION_UP = 'up';
	const DIRECTION_DOWN = 'down';
	const DIRECTION_STAND = 'stand';
	
	const DOOR_OPEN = 'open';
	const DOOR_CLOSED = 'closed';
	
	const FLOOR_COUNT = 10; //Количество этаже в здании
	
    public static function tableName()
    {
        return 'elevator';
    }

    public static function getElevators()
	{	
	    $elevators = self::find()->all();
		
		if (empty($elevators)) { //Если в БД нет лифтов, создаем 4 лифта
			for ($i = 0; $i < 4; $i++) {
				$elevator = new Elevator();
				$elevator->setDefaultState();
				$elevator->save();
				$elevators[] = $elevator;
			}
		}
	
		foreach($elevators as $key => $elevator)
		{		
			$elevators[$key] = $elevator->updateState();
		}
		
		return $elevators;
	}
	
	public static function findNearestElevatorId($from, $to)
	{
		//Опреляем направление движения в запросе пользователя
		if ($from < $to) {
			$direction = self::DIRECTION_UP;
			$whereConditionFromFloor = ['<', 'current_floor', $from]; //Текущий этаж лифта должен быть ниже этажа пользователя при движении вверх
			$whereConditionToFloor = ['>=', 'elevator_request.to_floor', $from]; //Этаж, на который поедет лифт, должен быть выше или равен этажу пользователя
		} else {
			$direction = self::DIRECTION_DOWN;
			$whereConditionFromFloor = ['>', 'current_floor', $from]; //Текущий этаж лифта должен быть выше этажа пользователя при движении вниз
			$whereConditionToFloor = ['<=', 'elevator_request.to_floor', $from]; //Этаж, на который поедет лифт, должен быть ниже или равен этажу пользователя
		}
		
		//Ищем лифты, которые стоят на этаже пользователя, т.к.
		//в приоритете минимальное время ожидания лифта пользователем
		$standingElevatorsAtUserFloor = self::find()->where(['direction' => self::DIRECTION_STAND, 'door' => self::DOOR_CLOSED, 'current_floor' => $from])->all();
		
		//Если есть хоть один лифт на этаже пользователя, то остальные лифты искать не нужно
		if (!empty($standingElevatorsAtUserFloor)) {
			$elevator = array_shift($standingElevatorsAtUserFloor);
			return $elevator->id;
		}
		
		//Выбираем остальные стоящие на месте лифты
		$standingElevators = self::find()
		   ->joinWith('requests')
		   ->where(['direction' => self::DIRECTION_STAND])
		   ->andWhere(['OR',
				['!=', 'elevator_request.processing', 1],
				['elevator_request.processing' => null]
		   ])
		   ->andWhere(['OR',
				$whereConditionToFloor,
				['elevator_request.to_floor' => null]
		   ])
		   ->all();
		   
		//Выбираем движущиеся в нужном направлении лифты
        //В выборку не попадут лифты, которые следуют с пассажирами в другом направлении, но финальная точка прибытия соответствует точке отправления
        //так как, на остановку по этажам для высадки и посадки пассажиров потребуется больше времени, чем вызвать пустой лифт, пусть и на более отдаленном этаже
		//в приоритете минимальное время ожидания лифта пользователем
		$movingElevators = self::find()
		   ->joinWith('requests')
		   ->where(['direction' => $direction])
		   ->andWhere($whereConditionFromFloor)
		   ->andWhere($whereConditionToFloor)
		   ->all();
		   
		$elevators = array_merge($movingElevators, $standingElevators); 
		$floorsBetween = self::FLOOR_COUNT;
		$theNearestElevatorId = 0;
		
		if (empty($elevators)) {
			throw new \ErrorException ("Все лифты заняты, Вам придется подождать. Может быть, воспользуетесь лестницей?");
		}
		
		//Ищем наиболее подходящий лифт
		foreach($elevators as $elevator) {
			if (abs($elevator->current_floor - $from) < $floorsBetween) {
				$floorsBetween = abs($elevator->current_floor - $from);
				$theNearestElevatorId = $elevator->id;
			}
		}
		
		return $theNearestElevatorId;
	}
	
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		    [['current_floor', 'door'], 'required'],
            [['current_floor'], 'number', 'min' => 1, 'max' => self::FLOOR_COUNT],
			[['direction'], 'string'],
			[['door'], 'string']
        ];
    }
 
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'current_floor' => 'Текущий этаж',
            'direction' => 'Направление движения',
            'door' => 'Двери'
        ];
    }
	
	/**
     * Обновление состояния лифта
     */
	public function updateState()
	{
		$requests = $this->requests;
		
		//Если у лифта нет вызовов, возвращаем неизмененное состояние лифта
		if (empty($requests)) {
			return $this;
		}
		
		//Лифт вызвали с этажа, отличного от того, где он стоял
		//Изменяем направление лифта
		if ($requests[0]->from_floor > $this->current_floor && $requests[0]->processing == 0) {
			$this->direction = self::DIRECTION_UP;
		} else if ($requests[0]->from_floor < $this->current_floor && $requests[0]->processing == 0) {
			$this->direction = self::DIRECTION_DOWN;
		}
		
		if ($this->direction == self::DIRECTION_UP) {
			$this->current_floor = $this->current_floor + 1;
		} else if ($this->direction == self::DIRECTION_DOWN) {
			$this->current_floor = $this->current_floor - 1;
		}
		
		foreach($requests as $request) {    
		    //Лифт приехал на этаж, на котором нужно сделать остановку
			if (($this->current_floor == $request->from_floor && $request->processing == 0) || $this->current_floor == $request->to_floor) {
				$requestDirection = ($request->from_floor < $request->to_floor) ? self::DIRECTION_UP : self::DIRECTION_DOWN;			
			
				if ($this->door == self::DOOR_OPEN) { //Если двери открыты, то закрываем их
				   $this->door = self::DOOR_CLOSED;
				   
				    if ($this->current_floor == $request->from_floor) {//Двери на этаже отправления закрыли, помечаем все запросы, стартующие с этого этажа - "в работе"
					   $request->startAllRequestsWithSuchStartStop();
					   $this->direction = $requestDirection;
				    }
				   
				    if ($this->current_floor == $request->to_floor) {//Двери на этаже прибытия закрыли, удаляем вызовы этого лифта
						$request->deleteAllRequestsWithSuchFinalStop();
						//Проверяем, конечная ли эта остановка лифта или нет
						//Если в лифте остались пассажиры, которых нужно доставить следующие этажи
						//Изменяем направление лифта, чтобы он продолжил движение
						$otherRequests = ElevatorRequest::getRequestsByElevatorId($this->id);			
						
						if (!empty($otherRequests)) {
							$this->direction = $requestDirection; 
							//Если есть вызовы, стартующие с этого этажа, сразу отмечаем их как выполняющиеся
							$currentFloor = $this->current_floor;
							$startAtThisFloorRequests = array_filter(
								$otherRequests,
								function ($e) use ($currentFloor) {
									return $e->from_floor == $currentFloor ? $e : null;
								}
							);

							if (!empty($startAtThisFloorRequests)) {
								$startAtThisFloorRequests[0]->startAllRequestsWithSuchStartStop();
							}							
						}
				    }
				} else if ($this->direction != self::DIRECTION_STAND) { //Иначе останавливаем лифт				
				   $this->direction = self::DIRECTION_STAND;				   
				} else { //Если лифт уже остановлен, открываем двери
				   $this->door = self::DOOR_OPEN;
				}
				
				//Сохраняем и возвращаем модель, т.к. за один вызов метода мы изменяем только один параметр состояния лифта
				$this->save();
				return $this;				
			}
		}	
		
		if (!$this->save()) {
			throw new \ErrorException ("Похоже, лифты сломались. Перезагрузите их.");
		}		
		
		return $this;
	}
	
	/**
     * Получение состояния лифта
     */
	public function getState()
    {
        return [
            'current_floor' => $this->current_floor,
            'door' => $this->door,
            'direction' => $this->direction
        ];
    }
	
	/**
     * Установка состояния по умолчанию
     */
	public function setDefaultState()
    {
		$this->current_floor = 1;
		$this->door = self::DOOR_CLOSED;
		$this->direction = self::DIRECTION_STAND;
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(ElevatorRequest::className(), ['elevator_id' => 'id']);
    }
    
}