<?php
 
namespace app\models;
 
use Yii;
 
class ElevatorRequest extends \yii\db\ActiveRecord
{	
    public static function tableName()
    {
        return 'elevator_request';
    }
	
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		    [['from_floor', 'to_floor', 'elevator_id'], 'required'],
            [['from_floor', 'to_floor'], 'number', 'min' => 1, 'max' => Elevator::FLOOR_COUNT],
			[['elevator_id'], 'integer'],
			[['processing'], 'boolean'], //Означает, что данный вызов сейчас выполняется
        ];
    }
	
	public function setState($from, $to)
	{
        $this->from_floor = $from;
        $this->to_floor = $to;
		$this->elevator_id = 0;
    }
	
	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from_floor' => 'Этаж отправления',
            'to_floor' => 'Этаж прибытия',
        ];
    }
	
	public function setElevatorId()
	{
		$this->elevator_id = Elevator::findNearestElevatorId($this->from_floor, $this->to_floor);
	}
    
	public function deleteAllRequestsWithSuchFinalStop()
	{
		return self::deleteAll(['elevator_id' => $this->elevator_id, 'to_floor' => $this->to_floor]);
	}
	
	public function startAllRequestsWithSuchStartStop()
	{
		return self::updateAll(['processing' => 1], 'elevator_id = ' . $this->elevator_id . ' AND from_floor = ' . $this->from_floor);
	}
	
	public static function getRequestsByElevatorId($elevatorId)
	{
		return self::findAll(['elevator_id' => $elevatorId]);
	}
}