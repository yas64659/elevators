<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Elevator;
use app\models\ElevatorRequest;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$elevatorRequest = new ElevatorRequest();
		$elevatorRequest->load(\Yii::$app->request->post());
		$from = $elevatorRequest->from_floor;
		$to = $elevatorRequest->to_floor;
		$message = '';
		
		//Поступил вызов
		if ($from != '' && $to != '') {
			if ($from == $to) {
				$message = "Вы уже находитесь на данном этаже";
			}
		
			$request = ElevatorRequest::findOne(['from_floor' => $from, 'to_floor' => $to, 'processing' => 0]);
		
		    //Если кто-то уже вызвал ли лифт ранее с такими же данными запроса 
			//и тоже его ждет, то вызов не дублируем
			if ($request != null) {
				$elevatorRequest = $request;
			} else {
				try {
					$elevatorRequest->setElevatorId();
					$elevatorRequest->save();
				} catch (\ErrorException $e) {
			        $message = $e->getMessage();
				}			    
			}			
		} else {
			//По умолчанию предлагаем пользователю проехать с 1-го по 10-ый этаж
			$elevatorRequest->setState(1, Elevator::FLOOR_COUNT); 
		}
		
		try {
			$elevators = Elevator::getElevators();
		} catch (\ErrorException $e) {
			$elevators = [];
			$message = $e->getMessage();
		}
		
		return $this->render('index', [
		    'floors' => Elevator::FLOOR_COUNT,
            'elevators' => $elevators,
			'elevatorRequest' => $elevatorRequest,
			'requests' => ElevatorRequest::find()->all(),
			'message' => $message
        ]);
    }
	
	public function actionReset()
    {
		$elevators = Elevator::find()->all();
		
		foreach($elevators as $key => $elevator)
		{		
			$elevator->setDefaultState();
			$elevator->save();
		}
		
		ElevatorRequest::deleteAll();
		return $this->redirect(['site/index']);
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
