<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Добро пожаловать в наш бизнес центр!';
?>
<div class="site-index">

    <div align="center">
        <h2>Добро пожаловать в наш Бизнес-центр!</h2>       
        <p><a class="btn btn-success" href="<?=Yii::$app->getUrlManager()->createUrl(['site/index'])?>">Обновить состояние</a> 
		<a class="btn btn-danger" href="<?=Yii::$app->getUrlManager()->createUrl(['site/reset'])?>">Перезагрузить лифты</a></p>
    </div>

    <div class="body-content" style="padding-top:30px;">

        <div class="row">
		
		<?php
			foreach($elevators as $elevator) {
		?>
		
			<div class="col-md-3">
				<div class="alert alert-info">
					<div><b>Этаж:</b> <?= $elevator->current_floor ?></div>
					<div><b>Движение:</b> <?= $elevator->direction ?></div>
					<div><b>Состояние дверей:</b> <?= $elevator->door ?></div>
				</div> 
				<div class="alert alert-warning" align="center"><p class="lead">Лифт <?= $elevator->id ?></p></div>
			</div>
		
		<?php
			}
		?>
            
        </div>
		
		<div class="row" align="center">
		<?php
			if ($message != '') {
		?>
			<div class="alert alert-danger"><?= $message ?></div>
		<?php
			} else if ($elevatorRequest->elevator_id == 0) {
		?>
			<p class="lead">Пожалуйста, выберите этаж, на котором Вы находитесь, и этаж, на который Вас необходимо доставить.</p>
		<?php
			} else {
		?>
		    <div class="alert alert-success">Ваш лифт: <?= $elevatorRequest->elevator_id ?></div>
		<?php
			}
		?>
		</div>
		
		<div class="row" align="center">
		<?php 
		$form = ActiveForm::begin([
			'action' => ['site/index'],
		]); 
		$listData = [];
		
		for ($i = 1; $i <= $floors; $i++) {
			$listData[$i] = $i;
		}
		
		?>
        <?= $form->field($elevatorRequest, 'from_floor')->dropDownList($listData, ['style' => 'width:200px']) ?>
		<?= $form->field($elevatorRequest, 'to_floor')->dropDownList($listData, ['style' => 'width:200px']) ?>
		<?= Html::submitButton('Вызов', ['class' => 'btn btn-success']) ?>
		<?php ActiveForm::end() ?>
		</div>
		<hr>
		<h3>Вызовы:</h3>
		<table class="table table-striped">
		<tr>
			<th>№ лифта</th>
			<th>Этаж отправления</th>
			<th>Этаж прибытия</th>
			<th>Статус вызова</th>
		</tr>
		
		<?php
			foreach($requests as $request) {
		?>
		
		<tr>
			<td><?= $request->elevator_id ?></td>
			<td><?= $request->from_floor ?></td>
			<td><?= $request->to_floor ?></td>
			<td><?= $request->processing == 0 ? 'Ожидает' : 'Выполняется' ?></td>
		</tr>
		
		<?php
			}
		?>
		
		</table>

    </div>
</div>
